﻿
namespace Exam_Zhuravlev
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_start = new System.Windows.Forms.Button();
            this.la_try = new System.Windows.Forms.Label();
            this.btn_timer = new System.Windows.Forms.Button();
            this.la_level = new System.Windows.Forms.Label();
            this.btn_level = new System.Windows.Forms.Button();
            this.btn_try = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_start
            // 
            this.btn_start.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_start.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_start.Location = new System.Drawing.Point(12, 12);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(223, 46);
            this.btn_start.TabIndex = 10;
            this.btn_start.Text = "Начать игру";
            this.btn_start.UseVisualStyleBackColor = false;
            // 
            // la_try
            // 
            this.la_try.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.la_try.AutoSize = true;
            this.la_try.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.la_try.Location = new System.Drawing.Point(580, 21);
            this.la_try.Name = "la_try";
            this.la_try.Size = new System.Drawing.Size(276, 26);
            this.la_try.TabIndex = 9;
            this.la_try.Text = "Чисо оставшихся попыток:";
            // 
            // btn_timer
            // 
            this.btn_timer.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_timer.Enabled = false;
            this.btn_timer.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_timer.Location = new System.Drawing.Point(291, 12);
            this.btn_timer.Name = "btn_timer";
            this.btn_timer.Size = new System.Drawing.Size(223, 46);
            this.btn_timer.TabIndex = 12;
            this.btn_timer.Text = "00:00";
            this.btn_timer.UseVisualStyleBackColor = false;
            // 
            // la_level
            // 
            this.la_level.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.la_level.AutoSize = true;
            this.la_level.Font = new System.Drawing.Font("Sitka Text", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.la_level.Location = new System.Drawing.Point(580, 72);
            this.la_level.Name = "la_level";
            this.la_level.Size = new System.Drawing.Size(208, 28);
            this.la_level.TabIndex = 13;
            this.la_level.Text = "Уровень сложности:";
            // 
            // btn_level
            // 
            this.btn_level.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_level.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_level.Enabled = false;
            this.btn_level.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_level.Location = new System.Drawing.Point(884, 64);
            this.btn_level.Name = "btn_level";
            this.btn_level.Size = new System.Drawing.Size(88, 46);
            this.btn_level.TabIndex = 14;
            this.btn_level.UseVisualStyleBackColor = false;
            // 
            // btn_try
            // 
            this.btn_try.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_try.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_try.Enabled = false;
            this.btn_try.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_try.Location = new System.Drawing.Point(884, 12);
            this.btn_try.Name = "btn_try";
            this.btn_try.Size = new System.Drawing.Size(88, 46);
            this.btn_try.TabIndex = 15;
            this.btn_try.UseVisualStyleBackColor = false;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 611);
            this.Controls.Add(this.btn_try);
            this.Controls.Add(this.btn_level);
            this.Controls.Add(this.la_level);
            this.Controls.Add(this.btn_timer);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.la_try);
            this.Name = "Fm";
            this.Text = "Лампочки";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Label la_try;
        private System.Windows.Forms.Button btn_timer;
        private System.Windows.Forms.Label la_level;
        private System.Windows.Forms.Button btn_level;
        private System.Windows.Forms.Button btn_try;
    }
}

