﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exam_Zhuravlev
{
    public partial class Fm : Form
    {
        Game game;
        int seconds = 50;
        public Fm()
        {

            InitializeComponent();
            game = new Game(this.Height, seconds);
            game.createLapsPattern(Controls, TYPE.READY);
            game.placeLamps(Controls, TYPE.READY);
            game.doCells(Controls, TYPE.READY, ACTION.LOCK);
            game.Refresh += Game_Refresh;
            game.NextStep += Game_NextStep;
            game.Lose_Game += Game_Lose_Game;
            game.Win_Game += Game_Win_Game;
            btn_start.Click += Btn_start_Click;
        }

        private void Game_Win_Game(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate {
                    btn_timer.Text = "Win!";
                }));
            }
            else
                btn_timer.Text = "Win";
        }

        private void Game_NextStep(object sender, EventArgs e)
        {
            seconds = 50;
            game.nextStep(Controls);
        }

        private void Game_Lose_Game(object sender, EventArgs e)
        {

            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate {
                    btn_timer.Text = "Timeout";
                }));
            }
            else
                btn_timer.Text = "Timeout";
        }

        private void Btn_start_Click(object sender, EventArgs e)
        {
            game.startGame();
        }

        private void Game_Refresh(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate {
                    btn_timer.Text = $"{game.seconds}";
                }));
            }
            else
                btn_timer.Text = $"{game.seconds}";
        }
    }
}
