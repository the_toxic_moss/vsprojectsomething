﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Exam_Zhuravlev
{

    class Game
    {
        public int size = 5, offset = 150, lampQ = 15;
        private int fHeight;
        Lamps[,] lamps, emptyLamps;
        public System.Timers.Timer timer;
        public System.Timers.Timer action;
        public int seconds;
        public event EventHandler Lose_Game;
        public event EventHandler NextStep;
        public event EventHandler Win_Game;
        public event EventHandler Refresh;
        public Game (int height, int sec)
        {
            lamps = new Lamps[size, size];
            seconds = sec;
            emptyLamps = new Lamps[size, size];
            fHeight = height;
            timer = new System.Timers.Timer();
            action = new System.Timers.Timer();
            timer.Interval = 100;
            timer.Elapsed += Timer_Elapsed;
            action.Interval = 1000;
            action.Elapsed += Action_Elapsed;
        }

        private void Action_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            seconds -= 1;
            Refresh?.Invoke(this, EventArgs.Empty);
            if (seconds == 0)
            {
                action.Stop();
                if (checkwin())
                    Win_Game?.Invoke(this, EventArgs.Empty);
            }
        }

        private bool checkwin()
        {
            return lamps == emptyLamps;
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            seconds -= 1;
            Refresh?.Invoke(this, EventArgs.Empty);
            if (seconds == 0)
            {
                timer.Stop();

                NextStep?.Invoke(this, EventArgs.Empty);
            }
        }

        public void nextStep(Control.ControlCollection cnt)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    cnt.Remove(lamps[i, j]);
                }
            }
            placeLamps(cnt, TYPE.EMPTY);
            action.Start();
        }

        public void createLapsPattern (Control.ControlCollection cnt, TYPE type)
        {

            Random rnd = new Random();

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Lamps newLamp = new Lamps();
                    newLamp.Size = new Size((fHeight - offset) / size, (fHeight - offset) / size);
                    newLamp.Location = new Point(i * (fHeight - offset) / size + 30, j * (fHeight - offset) / size + 70);
                    
                    switch (type)
                    {
                        case TYPE.READY:
                            {
                            if (lampQ >= rnd.Next(0, 100))
                                {
                                newLamp.BackColor = Color.Green;
                                newLamp.isOn = true;
                                }
                            else
                                {
                                newLamp.BackColor = Color.LightSlateGray;
                                newLamp.isOn = false;
                                }
                            
                                lamps[i, j] = newLamp;
                            }
                            break;
                        case TYPE.EMPTY:
                            {
                                newLamp.BackColor = Color.LightSlateGray;
                                newLamp.isOn = false;
                                emptyLamps[i, j] = newLamp;
                            break;
                            }
                            
                    }


                }

            }
        }

        public void placeLamps(Control.ControlCollection cnt, TYPE type)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    switch (type)
                    {
                        case TYPE.READY:
                            {
                                lamps[i,j].MouseUp += NewLamp_MouseUp;
                                cnt.Add(lamps[i, j]);
                            }
                            break;
                        case TYPE.EMPTY:
                            {
                                emptyLamps[i, j].MouseUp += NewLamp_MouseUp;
                                cnt.Add(emptyLamps[i, j]);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void NewLamp_MouseUp(object sender, MouseEventArgs e)
        {
            Lamps clickedLamp = (Lamps)sender;
            if (e.Button == MouseButtons.Left)
            {
                clickedLamp.BackColor = Color.Yellow;
                clickedLamp.isOn = true;
            }
            if (e.Button == MouseButtons.Right)
            {
                clickedLamp.BackColor = Color.LightSlateGray;
                clickedLamp.isOn = false;
            }
        }


        public void startGame()
        {
            timer.Start();
        }
        public void doCells(Control.ControlCollection cnt, TYPE type, ACTION action)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    switch (type)
                    {
                        case TYPE.READY:
                            {
                                if (action == ACTION.LOCK)
                                    lamps[i, j].Enabled = false;
                                else
                                    lamps[i, j].Enabled = true;
                            }
                            break;
                        case TYPE.EMPTY:
                            {
                                if (action == ACTION.LOCK)
                                    emptyLamps[i, j].Enabled = false;
                                else
                                    emptyLamps[i, j].Enabled = true;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
    public class Lamps: Button
    {
        public bool isOn;
    }

    public enum TYPE { READY, EMPTY };
    public enum ACTION { LOCK, UNLOCK};
}
