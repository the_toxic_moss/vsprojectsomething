﻿
namespace IndividualProject_1
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label = new System.Windows.Forms.Label();
            this.btn_ng = new System.Windows.Forms.Button();
            this.btn_pause = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Swis721 BT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label.Location = new System.Drawing.Point(12, 9);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(85, 22);
            this.label.TabIndex = 0;
            this.label.Text = "Score: 0";
            // 
            // btn_ng
            // 
            this.btn_ng.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_ng.Font = new System.Drawing.Font("Swis721 BT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_ng.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_ng.Location = new System.Drawing.Point(439, 5);
            this.btn_ng.Name = "btn_ng";
            this.btn_ng.Size = new System.Drawing.Size(157, 36);
            this.btn_ng.TabIndex = 1;
            this.btn_ng.Text = "New Game";
            this.btn_ng.UseVisualStyleBackColor = false;
            // 
            // btn_pause
            // 
            this.btn_pause.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_pause.Font = new System.Drawing.Font("Swis721 BT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_pause.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_pause.Location = new System.Drawing.Point(602, 5);
            this.btn_pause.Name = "btn_pause";
            this.btn_pause.Size = new System.Drawing.Size(100, 36);
            this.btn_pause.TabIndex = 2;
            this.btn_pause.Text = "Pause";
            this.btn_pause.UseVisualStyleBackColor = false;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(714, 691);
            this.Controls.Add(this.btn_pause);
            this.Controls.Add(this.btn_ng);
            this.Controls.Add(this.label);
            this.MaximumSize = new System.Drawing.Size(730, 730);
            this.MinimumSize = new System.Drawing.Size(730, 730);
            this.Name = "Fm";
            this.Text = "PacMan";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Button btn_ng;
        private System.Windows.Forms.Button btn_pause;
    }
}

