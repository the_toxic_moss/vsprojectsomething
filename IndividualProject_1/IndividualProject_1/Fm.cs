﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IndividualProject_1
{
    public partial class Fm : Form
    {
        private GameField GF;
        private Game game;

        private readonly Graphics g;
        private readonly Bitmap b;

        public Fm()
        {
            InitializeComponent();
            KeyPreview = true;
            DoubleBuffered = true;
            b = new Bitmap(this.Width, this.Height);
            g = Graphics.FromImage(b);
            g.TranslateTransform(this.Width / 2 - 10, this.Height / 2 - 20);

            GF = new GameField();
            game = new Game(GF);

            btn_pause.Click += gamePause;
            btn_ng.Click += gameRestart;
            KeyUp += Fm_KeyUp;
            this.Paint += (s, e) => game.draw(e.Graphics, b);
            label.Text = $"Score: {game.score}";
            initGame();
            this.Invalidate();
        }

        private void gameRestart(object sender, EventArgs e)
        {
            game.restart(new GameField(), g);
        }

        private void gamePause(object sender, EventArgs e)
        {
            if (game.isPaused == false)
            {
                game.stopGhosts();
                game.pacmanInterval.Stop();
                game.isPaused = true;
                btn_pause.Text = "Continue";
            }
            else
            {
                game.pacmanInterval.Start();
                game.releaseGhosts();
                game.isPaused = false;
                btn_pause.Text = "Pause";
            }
        }

        private void initGame()
        {
            game.Refresh += Game_Refresh;
            game.ClearPacman += Game_ClearPacman;
            game.RefreshPacman += Game_RefreshPacman;
            game.ClearGhosts += Game_ClearGhosts;
            game.RefreshGhosts += Game_RefreshGhosts;
            game.Restart += Game_Restart;
            game.drawWalls(g);
            game.drawDots(g);
            game.drawPacman(g);
            game.findGhosts();
            game.drawGhosts(g);
            game.ghostMoveTo();
        }

        private void Game_Restart(object sender, EventArgs e)
        {
            game.restart(new GameField(), g);
        }

        private void Game_RefreshGhosts(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate {
                    game.drawGhosts(g);
                }));
            }
            else
                game.drawGhosts(g);
        }

        private void Game_ClearGhosts(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate {
                    game.eraseGhost(g);
                }));
            }
            else
                game.eraseGhost(g);
        }


        private void Game_ClearPacman(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate {
                    game.erasePacman(g);
                }));
            }
            else
                game.erasePacman(g);
        }

        private void Game_RefreshPacman(object sender, EventArgs e)
        {
            if (label.InvokeRequired)
            {
                label.Invoke(new MethodInvoker(delegate{label.Text = $"Score: {game.score}"; }));
            }
            else
            {
                label.Text = $"Score: {game.score}";
            }

            if (this.InvokeRequired)
            {
                label.Invoke(new MethodInvoker(delegate { game.drawPacman(g); }));
            }
            else
                game.drawPacman(g);
        }


        private void Game_Refresh(object sender, EventArgs e)
        {
            this.Invalidate();
        }

        private void Fm_KeyUp(object sender, KeyEventArgs e)
        {   if(!game.isPaused)
                switch (e.KeyCode)
                {
                    case Keys.A:
                        if (game.checkPacmanMovement(MOVEMENT.LEFT))
                            game.pacmanMoveTo(MOVEMENT.LEFT);
                        break;
                    case Keys.D:
                        if (game.checkPacmanMovement(MOVEMENT.RIGHT))
                            game.pacmanMoveTo(MOVEMENT.RIGHT);
                        break;
                    case Keys.W:
                        if (game.checkPacmanMovement(MOVEMENT.UP))
                            game.pacmanMoveTo(MOVEMENT.UP);
                        break;
                    case Keys.S:
                        if (game.checkPacmanMovement(MOVEMENT.DOWN))
                            game.pacmanMoveTo(MOVEMENT.DOWN);
                        break;
                }
        }
    }
}



