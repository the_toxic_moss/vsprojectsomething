﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;

namespace IndividualProject_1
{
    class Game
    {
        public event EventHandler Refresh;
        public event EventHandler RefreshPacman;
        public event EventHandler RefreshGhosts;
        public event EventHandler ClearPacman;
        public event EventHandler ClearGhosts;
        public event EventHandler Restart;
        private GameField GF;
        public int score = 0;
        private Pacman pacman;
        private Ghost blinky = new Ghost();
        private Ghost pinky = new Ghost();
        private Ghost inky = new Ghost();
        private Ghost clyde = new Ghost();
        public System.Timers.Timer pacmanInterval = new System.Timers.Timer();

        public bool isPaused;
        public Game(GameField GF) 
        {
            this.GF = GF;
            pacman = new Pacman();
            pacmanInterval.Interval = 120;
            pacmanInterval.Elapsed += PacmanInterval_Elapsed;
            blinky.interval.Interval = 125;
            blinky.interval.Elapsed += BlinkyInterval_Elapsed;
            pinky.interval.Interval = 135;
            pinky.interval.Elapsed += PinkyInterval_Elapsed;
            inky.interval.Interval = 135;
            inky.interval.Elapsed += InkyInterval_Elapsed; 
            clyde.interval.Interval = 130;
            clyde.interval.Elapsed += ClydeInterval_Elapsed;
            blinky.Death += Blinky_death;
            pacman.Teleport += Pacman_Teleport;
            blinky.Teleport += Blinky_Teleport;
            blinky.color = Color.Red;
            blinky.type = OBJECT_LIST.BLINKY;
            pinky.color = Color.Pink;
            pinky.type = OBJECT_LIST.PINKY;
            inky.color = Color.Gray;
            inky.type = OBJECT_LIST.INKY;
            clyde.color = Color.LightCoral;
            clyde.type = OBJECT_LIST.CLYDE;
        }

        internal void releaseGhosts()
        {
            blinky.interval.Start();
        }

        internal void stopGhosts()
        {
            blinky.interval.Stop();
        }

        private void Blinky_Teleport(object sender, EventArgs e)
        {
            ClearGhosts?.Invoke(this, EventArgs.Empty);
            blinky.teleportationSetup(GF.FIELD);
        }

        private void Pacman_Teleport(object sender, EventArgs e)
        {
            ClearPacman?.Invoke(this, EventArgs.Empty);
            pacman.teleportationSetup(GF.FIELD);
        }

        private void Blinky_death(object sender, EventArgs e)
        {
            Restart?.Invoke(this, EventArgs.Empty);
        }

        private void ClydeInterval_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //if (clyde.ghostCanMove(GF.FIELD, clyde.moveDirection))
            //{
            //    ClearGhosts?.Invoke(this, EventArgs.Empty);
            //    checkTarget(clyde);
            //    clyde.updateGhostCell(GF.FIELD);
            //    RefreshGhosts?.Invoke(this, EventArgs.Empty);
            //    Refresh?.Invoke(this, EventArgs.Empty);
            //}
            //else clyde.interval.Stop();
        }

        private void InkyInterval_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //if (inky.ghostCanMove(GF.FIELD, inky.moveDirection))
            //{
            //    ClearGhosts?.Invoke(this, EventArgs.Empty);
            //    checkTarget(inky);
            //    inky.updateGhostCell(GF.FIELD);
            //    RefreshGhosts?.Invoke(this, EventArgs.Empty);
            //    Refresh?.Invoke(this, EventArgs.Empty);
            //}
            //else inky.interval.Stop();
        }

        private void PinkyInterval_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //if (pinky.ghostCanMove(GF.FIELD, pinky.moveDirection))
            //{
            //    ClearGhosts?.Invoke(this, EventArgs.Empty);
            //    checkTarget(pinky);
            //    pinky.updateGhostCell(GF.FIELD);
            //    RefreshGhosts?.Invoke(this, EventArgs.Empty);
            //    Refresh?.Invoke(this, EventArgs.Empty);
            //}
            //else pinky.interval.Stop();
        }

        private void BlinkyInterval_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (blinky.ghostCanMove(GF.FIELD, blinky.moveDirection))
            {
                ClearGhosts?.Invoke(this, EventArgs.Empty);
                checkTarget(blinky);
                blinky.updateGhostCell(GF.FIELD);
                RefreshGhosts?.Invoke(this, EventArgs.Empty);
                Refresh?.Invoke(this, EventArgs.Empty);
            }
            else 
                checkTarget(blinky);
            
        }

        private void checkTarget(Ghost ghost)
        {
            switch (ghost.type)
            {
                case OBJECT_LIST.BLINKY:
                    takeDown(ghost.iCell, ghost.jCell, pacman.iCell, pacman.jCell, blinky);
                    break;
                case OBJECT_LIST.PINKY:
                    switch (pacman.moveDirection)
                    {
                        case MOVEMENT.UP:
                            takeDown(ghost.iCell, ghost.jCell, pacman.iCell - 4, pacman.jCell, pinky);
                            break;
                        case MOVEMENT.DOWN:
                            takeDown(ghost.iCell, ghost.jCell, pacman.iCell + 4, pacman.jCell, pinky);
                            break;
                        case MOVEMENT.LEFT:
                            takeDown(ghost.iCell, ghost.jCell, pacman.iCell, pacman.jCell - 4, pinky);
                            break;
                        case MOVEMENT.RIGHT:
                            takeDown(ghost.iCell, ghost.jCell, pacman.iCell, pacman.jCell + 4, pinky);
                            break;
                        default:
                            break;
                    }
                    break;
                case OBJECT_LIST.INKY:
                    switch (pacman.moveDirection)
                    {
                        case MOVEMENT.UP:
                            takeDown(ghost.iCell, ghost.jCell, (pacman.iCell - 2 - blinky.iCell) * 2, (pacman.jCell - blinky.jCell) * 2, inky);
                            break;
                        case MOVEMENT.DOWN:
                            takeDown(ghost.iCell, ghost.jCell, (pacman.iCell + 2 - blinky.iCell) * 2, (pacman.jCell - blinky.jCell) * 2, inky);
                            break;
                        case MOVEMENT.LEFT:
                            takeDown(ghost.iCell, ghost.jCell, (pacman.iCell - blinky.iCell) * 2, (pacman.jCell - 2 - blinky.jCell) * 2, inky);
                            break;
                        case MOVEMENT.RIGHT:
                            takeDown(ghost.iCell, ghost.jCell, (pacman.iCell - blinky.iCell) * 2, (pacman.jCell + 2 - blinky.jCell) * 2, inky);
                            break;
                        default:
                            break;
                    }
                    break;
                case OBJECT_LIST.CLYDE:
                    takeDown(ghost.iCell, ghost.jCell, clydeTarget().i, clydeTarget().j, clyde);
                    break;
                default:
                    break;
            }
        }

        private (int i, int j) clydeTarget()
        {
            
            if ((int)Math.Round(Math.Sqrt(Math.Pow(clyde.iCell - pacman.jCell, 2) + Math.Pow(clyde.jCell - pacman.jCell, 2))) > 8)
            {
                return (pacman.iCell, pacman.jCell);
            }
            else
            {
                return (0, 0);
            }
        }

        private void takeDown(int gi, int gj, int ti, int tj, Ghost ghost)
        {
            int counter = 0;
            int leftCell = 999, rightCell = 999, topCell = 999, downCell = 999;

            if ((GF.FIELD[gi,gj - 1] == 0 || GF.FIELD[gi,gj - 1] == 4) && (gj - 1) != ghost.prevPoint.j)
            {
                leftCell = (int)Math.Round(Math.Sqrt(Math.Pow(ti - (gi), 2) + Math.Pow(tj - (gj - 1), 2)));
                ++counter;
            }
            if ((GF.FIELD[gi,gj + 1] == 0 || GF.FIELD[gi,gj + 1] == 4) && (gj + 1) != ghost.prevPoint.j)
            {
                rightCell = (int)Math.Round(Math.Sqrt(Math.Pow(ti - (gi), 2) + Math.Pow(tj - (gj + 1), 2)));
                ++counter;
            }
            if ((GF.FIELD[gi - 1, gj] == 0 || GF.FIELD[gi - 1,gj] == 4) && (gi - 1) != ghost.prevPoint.i)
            {
                topCell = (int)Math.Round(Math.Sqrt(Math.Pow(ti - (gi - 1), 2) + Math.Pow(tj - (gj), 2)));
                ++counter;
            }
            if ((GF.FIELD[gi + 1,gj] == 0 || GF.FIELD[gi + 1,gj] == 4) && (gi + 1) != ghost.prevPoint.i)
            {
                downCell = (int)Math.Round(Math.Sqrt(Math.Pow(ti - (gi + 1), 2) + Math.Pow(tj - (gj), 2)));
                ++counter;
            }


            if (counter >= 1)
            {
                var smallest = (new List<int>() {leftCell, rightCell, topCell, downCell}).Min();
                if (smallest == topCell && smallest != 999 && checkGhostMovement(MOVEMENT.UP, ghost))
                {
                    ghost.moveDirection = MOVEMENT.UP;
                }
                else if (smallest == downCell && smallest != 999 && checkGhostMovement(MOVEMENT.DOWN, ghost))
                {
                    ghost.moveDirection = MOVEMENT.DOWN;
                }
                else if (smallest == leftCell && smallest != 999 && checkGhostMovement(MOVEMENT.LEFT, ghost))
                {
                    ghost.moveDirection = MOVEMENT.LEFT;
                }
                else if (smallest == rightCell && smallest != 999 && checkGhostMovement(MOVEMENT.RIGHT, ghost))
                {
                    ghost.moveDirection = MOVEMENT.RIGHT;
                }
            }
            else
            {
                ghost.prevPoint = (ghost.iCell, ghost.jCell);
            }

        }

        private void PacmanInterval_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (pacman.pacmanCanMove(GF.FIELD, pacman.moveDirection))
            {
                ClearPacman?.Invoke(this, EventArgs.Empty);
                checkScore();
                pacman.updatePacmanCell(GF.FIELD);
                RefreshPacman?.Invoke(this, EventArgs.Empty);
                Refresh?.Invoke(this, EventArgs.Empty);
            }
            else
            {
                pacmanInterval.Stop();
            }
            //else
            //if (OBJECT_LIST[GF.FIELD[pacman.iCell - y, pacman.jCell + x]] == OBJECT_LIST.BLINKY)
          
        }

        private void checkScore()
        {
            switch (pacman.moveDirection)
            {
                case MOVEMENT.UP:
                    checkFood(0, 1);
                    break;
                case MOVEMENT.DOWN:
                    checkFood(0, -1);
                    break;
                case MOVEMENT.LEFT:
                    checkFood(-1, 0);
                    break;
                case MOVEMENT.RIGHT:
                    checkFood(1, 0);
                    break;
            }
            
        }


        private void checkFood(int x, int y)
        {
            if (GF.FIELD[pacman.iCell - y, pacman.jCell + x] == (int)OBJECT_LIST.DOT)
                score++;
        }


        public void restart(GameField gameField, Graphics g)
        {
            this.GF = gameField;
            erasePacman(g);
            eraseGhost(g);
            findGhosts();
            score = 0;
            RefreshPacman?.Invoke(this, EventArgs.Empty);
            RefreshGhosts?.Invoke(this, EventArgs.Empty);
            drawDots(g);
            pacmanInterval.Stop();
            blinky.moveDirection = MOVEMENT.NONE;
            pacman.moveDirection = MOVEMENT.NONE;
            Refresh?.Invoke(this, EventArgs.Empty);
        }

        public bool checkPacmanMovement(MOVEMENT m)
        {
            return m != pacman.moveDirection && pacman.pacmanCanMove(GF.FIELD, m);
        }

        private bool checkGhostMovement(MOVEMENT m, Ghost ghost)
        {
            return m != ghost.moveDirection && ghost.ghostCanMove(GF.FIELD, m);
        }

        public void erasePacman(Graphics g)
        {
            g.FillEllipse(new SolidBrush(Color.Black),
                pacman.jCell * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - pacman.size / 2,
                (pacman.iCell) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - pacman.size / 2,
                pacman.size, pacman.size);
        }

        public void eraseGhost( Graphics g)
        {
            g.FillRectangle(new SolidBrush(Color.Black),
                blinky.jCell * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - blinky.size / 2,
                (blinky.iCell) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - blinky.size / 2,
                blinky.size, blinky.size);

            if (blinky.prevVal == (int)OBJECT_LIST.DOT)
            {
                g.FillEllipse(new SolidBrush(Color.White),
                blinky.jCell * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - GF.DOT_SIZE / 2,
                (blinky.iCell) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - GF.DOT_SIZE / 2,
                GF.DOT_SIZE, GF.DOT_SIZE);
            }

            //g.FillRectangle(new SolidBrush(Color.Black),
            //    pinky.jCell * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - pinky.size / 2,
            //    (pinky.iCell) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - pinky.size / 2,
            //    pinky.size, pinky.size);

            //if (pinky.prevVal == (int)OBJECT_LIST.DOT)
            //{
            //    g.FillEllipse(new SolidBrush(Color.White),
            //    pinky.prevPoint.j * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - GF.DOT_SIZE / 2,
            //    (pinky.prevPoint.i) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - GF.DOT_SIZE / 2,
            //    GF.DOT_SIZE, GF.DOT_SIZE);
            //}

            //g.FillRectangle(new SolidBrush(Color.Black),
            //    inky.jCell * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - inky.size / 2,
            //    (inky.iCell) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - inky.size / 2,
            //    inky.size, inky.size);

            //if (inky.prevVal == (int)OBJECT_LIST.DOT)
            //{
            //    g.FillEllipse(new SolidBrush(Color.White),
            //    inky.prevPoint.j * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - GF.DOT_SIZE / 2,
            //    (inky.prevPoint.i) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - GF.DOT_SIZE / 2,
            //    GF.DOT_SIZE, GF.DOT_SIZE);
            //}

            //g.FillRectangle(new SolidBrush(Color.Black),
            //    inky.jCell * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - inky.size / 2,
            //    (inky.iCell) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - inky.size / 2,
            //    inky.size, inky.size);

            //if (clyde.prevVal == (int)OBJECT_LIST.DOT)
            //{
            //    g.FillEllipse(new SolidBrush(Color.White),
            //    clyde.prevPoint.j * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - GF.DOT_SIZE / 2,
            //    (clyde.prevPoint.i) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - GF.DOT_SIZE / 2,
            //    GF.DOT_SIZE, GF.DOT_SIZE);
            //}
        }

        public void draw(Graphics g, Bitmap b)
        {
            g.DrawImage(b, 0, 0);
        }

        public void drawPacman(Graphics g)
        {
            var pacmanPos = findObject((int)OBJECT_LIST.PACMAN).First();
            pacman.iCell = pacmanPos.i;
            pacman.jCell = pacmanPos.j;
            g.FillEllipse(new SolidBrush(Color.Yellow),
                pacman.jCell * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - pacman.size / 2,
                (pacman.iCell) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - pacman.size / 2,
                pacman.size, pacman.size);
           
        }

        public void findGhosts()
        {
            var blinkyPos = findObject((int)OBJECT_LIST.BLINKY).First();
            //var inkyPos = findObject((int)OBJECT_LIST.INKY).First();
            //var pinkyPos = findObject((int)OBJECT_LIST.PINKY).First();
            //var clydePos = findObject((int)OBJECT_LIST.CLYDE).First();
            blinky.iCell = blinkyPos.i;
            blinky.jCell = blinkyPos.j;
            blinky.prevPoint = (blinky.iCell - 1, blinky.jCell);
            //pinky.iCell = pinkyPos.i;
            //pinky.jCell = pinkyPos.j;
            //inky.iCell = inkyPos.i;
            //inky.jCell = inkyPos.j;
            //clyde.iCell = clydePos.i;
            //clyde.jCell = clydePos.j;
        }

        public void drawGhosts(Graphics g)
        {
            g.FillRectangle(new SolidBrush(blinky.color),
               blinky.jCell * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - blinky.size / 2,
               (blinky.iCell) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - blinky.size / 2,
               blinky.size, blinky.size);
            //g.FillRectangle(new SolidBrush(pinky.color),
            //   pinky.jCell * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - pinky.size / 2,
            //   (pinky.iCell) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - pinky.size / 2,
            //   pinky.size, pinky.size);
            //g.FillRectangle(new SolidBrush(inky.color),
            //   inky.jCell * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - inky.size / 2,
            //   (inky.iCell) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - inky.size / 2,
            //   inky.size, inky.size);
            //g.FillRectangle(new SolidBrush(clyde.color),
            //   clyde.jCell * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - clyde.size / 2,
            //   (clyde.iCell) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - clyde.size / 2,
            //   clyde.size, clyde.size);


        }

        public void drawDots(Graphics g)
        {
            var dots = findObject((int)OBJECT_LIST.DOT);
            foreach (var dot in dots)
            {
                g.FillEllipse(new SolidBrush(Color.White),
                dot.j * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - GF.DOT_SIZE / 2,
                (dot.i) * GF.CELL_SIZE - (GF.FIELD_SIZE) + GF.CELL_SIZE / 2 - GF.DOT_SIZE/ 2,
                GF.DOT_SIZE, GF.DOT_SIZE);
            }
        }


        public void pacmanMoveTo(MOVEMENT move)
        {
            pacman.moveDirection = move;
            pacmanInterval.Start();
        }

        public void ghostMoveTo()
        {
            blinky.interval.Start();
            pinky.interval.Start();
            inky.interval.Start();
            clyde.interval.Start();
        }

        public void drawWalls(Graphics g)
        {
            int[,] checkedCells = new int[34, 34];
            List<Point> tempFigure = new List<Point>();
            foreach (var wall in findObject((int)OBJECT_LIST.WALL))
            {
                if (GF.FIELD[wall.i, wall.j] != checkedCells[wall.i, wall.j])
                {
                    checkedCells[wall.i, wall.j] = 2;
                    tempFigureAdd(tempFigure, wall.i, wall.j);
                    follow("left", wall.i, wall.j, tempFigure, checkedCells);
                    follow("right", wall.i, wall.j, tempFigure, checkedCells);
                    var figure = truncateFigure(tempFigure);
                    tempFigure = new List<Point>();
                    var shape = drawPath(figure);
                    g.FillPath(Brushes.Blue, shape);

                }
            }
            Refresh?.Invoke(this, EventArgs.Empty);
        }

        private GraphicsPath drawPath(List<Point> figure)
        {
            var head = figure[0];
            var path = new GraphicsPath();
            checkPath(head, figure, path);
            return path;
        }

        private void checkPath(Point head, List<Point> figure, GraphicsPath path)
        {
            var topCs = findPointAround(head.X, head.Y - (GF.CELL_SIZE - GF.WALL_SIZE), figure);
            var topWs = findPointAround(head.X, head.Y - (GF.WALL_SIZE), figure);
            var downCs = findPointAround(head.X, head.Y + (GF.CELL_SIZE - GF.WALL_SIZE), figure);
            var downWs = findPointAround(head.X, head.Y + (GF.WALL_SIZE), figure);
            var rightCs = findPointAround(head.X + (GF.CELL_SIZE - GF.WALL_SIZE), head.Y, figure);
            var rightWs = findPointAround(head.X + (GF.WALL_SIZE), head.Y, figure);
            var leftCs = findPointAround(head.X - (GF.CELL_SIZE - GF.WALL_SIZE), head.Y, figure);
            var leftWs = findPointAround(head.X - (GF.WALL_SIZE), head.Y, figure);

            List<Point> pointsAround = new List<Point>();
            pointsAround.AddRange(new Point[] { rightCs, rightWs, downCs, downWs, leftCs, leftWs, topCs, topWs });
            foreach (var item in pointsAround)
            {
                if (item != new Point(999, 999))
                {
                    figure.Remove(item);
                    path.AddLine(head, item);
                    checkPath(item, figure, path);
                    break;
                }
            }
        }

        private Point findPointAround(int x, int y, List<Point> figure)
        {
            var point = figure.Where(item => item.X == x && item.Y == y).FirstOrDefault();
            if (point.IsEmpty)
            {
                return new Point(999, 999);
            }
            else return point;
        }

        private List<Point> truncateFigure(List<Point> tempFigure)
        {
            List<Point> figure = new List<Point>();
            for (int i = 0; i < tempFigure.Count; i++)
            {
                int count = 0;
                var point = tempFigure[i];
                if (checkPointToSkip(tempFigure, point.X, point.Y + (GF.CELL_SIZE - GF.WALL_SIZE)) ||
                    checkPointToSkip(tempFigure, point.X, point.Y + (GF.WALL_SIZE)))
                    count++;
                if (checkPointToSkip(tempFigure, point.X, point.Y - (GF.CELL_SIZE - GF.WALL_SIZE)) ||
                    checkPointToSkip(tempFigure, point.X, point.Y - (GF.WALL_SIZE)))
                    count++;
                if (checkPointToSkip(tempFigure, point.X + (GF.CELL_SIZE - GF.WALL_SIZE), point.Y) ||
                    checkPointToSkip(tempFigure, point.X + (GF.WALL_SIZE), point.Y))
                    count++;
                if (checkPointToSkip(tempFigure, point.X - (GF.CELL_SIZE - GF.WALL_SIZE), point.Y) ||
                    checkPointToSkip(tempFigure, point.X - (GF.WALL_SIZE), point.Y))
                    count++;
                if (checkPointToSkip(tempFigure, point.X - (GF.CELL_SIZE - GF.WALL_SIZE), point.Y + (GF.CELL_SIZE - GF.WALL_SIZE)) ||
                    checkPointToSkip(tempFigure, point.X + (GF.CELL_SIZE - GF.WALL_SIZE), point.Y + (GF.CELL_SIZE - GF.WALL_SIZE)) ||
                    checkPointToSkip(tempFigure, point.X + (GF.CELL_SIZE - GF.WALL_SIZE), point.Y - (GF.CELL_SIZE - GF.WALL_SIZE)) ||
                    checkPointToSkip(tempFigure, point.X - (GF.CELL_SIZE - GF.WALL_SIZE), point.Y - (GF.CELL_SIZE - GF.WALL_SIZE)))
                    count++;
                if (count < 5)
                    figure.Add(tempFigure[i]);
            }

            return figure;
        }

        private bool checkPointToSkip(List<Point> tempFigure, int x, int y)
        {
            return tempFigure.Contains(new Point(x, y));
        }

        private void follow(string type, int i, int j, List<Point> tempFigure, int[,] checkedCells)
        {
            if (GF.FIELD[i, j] != 0)
            {
                if (type == "right" && j < 33)
                {
                    if (GF.FIELD[i, j + 1] == GF.FIELD[i, j] && GF.FIELD[i, j + 1] != checkedCells[i, j + 1])
                    {
                        tempFigureAdd(tempFigure, i, j + 1);
                        checkedCells[i, j + 1] = GF.FIELD[i, j + 1];
                        follow("right", i, j + 1, tempFigure, checkedCells);
                    }
                    else
                        follow("down", i, j, tempFigure, checkedCells);
                }
                if (type == "down" && i < 33)
                {
                    if (GF.FIELD[i + 1, j] == GF.FIELD[i, j] && GF.FIELD[i + 1, j] != checkedCells[i + 1, j])
                    {
                        tempFigureAdd(tempFigure, i + 1, j);
                        checkedCells[i + 1, j] = GF.FIELD[i + 1, j];
                        follow("down", i + 1, j, tempFigure, checkedCells);
                        follow("left", i + 1, j, tempFigure, checkedCells);
                        follow("right", i + 1, j, tempFigure, checkedCells);
                    }
                }
                if (type == "left" && j > 0)
                {
                    if (GF.FIELD[i, j - 1] == GF.FIELD[i, j] && GF.FIELD[i, j - 1] != checkedCells[i, j - 1])
                    {
                        tempFigureAdd(tempFigure, i, j - 1);
                        checkedCells[i, j - 1] = GF.FIELD[i, j - 1];
                        follow("left", i, j - 1, tempFigure, checkedCells);
                    }
                    else
                        follow("down", i, j, tempFigure, checkedCells);
                }
            }
        }

        private void tempFigureAdd(List<Point> tempFigure, int i, int j)
        {
            tempFigure.Add(new Point((j) * GF.CELL_SIZE - (GF.FIELD_SIZE) + (GF.CELL_SIZE / 2 - GF.WALL_SIZE / 2), (i) * GF.CELL_SIZE - (GF.FIELD_SIZE) + (GF.CELL_SIZE / 2 - GF.WALL_SIZE / 2)));
            tempFigure.Add(new Point((j) * GF.CELL_SIZE - (GF.FIELD_SIZE) + (GF.CELL_SIZE / 2 + GF.WALL_SIZE / 2), (i) * GF.CELL_SIZE - (GF.FIELD_SIZE) + (GF.CELL_SIZE / 2 - GF.WALL_SIZE / 2))); 
            tempFigure.Add(new Point((j) * GF.CELL_SIZE - (GF.FIELD_SIZE) + (GF.CELL_SIZE / 2 + GF.WALL_SIZE / 2), (i) * GF.CELL_SIZE - (GF.FIELD_SIZE) + (GF.CELL_SIZE / 2 + GF.WALL_SIZE / 2)));
            tempFigure.Add(new Point((j) * GF.CELL_SIZE - (GF.FIELD_SIZE) + (GF.CELL_SIZE / 2 - GF.WALL_SIZE / 2), (i) * GF.CELL_SIZE - (GF.FIELD_SIZE) + (GF.CELL_SIZE / 2 + GF.WALL_SIZE / 2)));
        }

        private List<(int i, int j)> findObject(int type)
        {
            List<(int, int)> array = new List<(int, int)>();
            for (int i = 0; i < 34; i++)
            {
                for (int j = 0; j < 34; j++)
                {
                    if (GF.FIELD[i, j] == type)
                    {
                        array.Add((i, j));
                    }
                }
            }
            return array;
        }
    }
}
