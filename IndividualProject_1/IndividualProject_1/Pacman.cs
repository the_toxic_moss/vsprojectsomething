﻿using System;
using System.Collections.Generic;


namespace IndividualProject_1
{
    class Pacman
    {
        public int iCell = 0;
        public int jCell = 0;
        public MOVEMENT moveDirection;
        public event EventHandler Teleport;
        //public Point posToMove = new Point();
        public int size = 18;

        public bool pacmanCanMove(int[,] field, MOVEMENT m)
        { 
            switch (m)
            {
                case MOVEMENT.UP:
                    return checkMovement(0, 1, field);
                case MOVEMENT.DOWN:
                    return checkMovement(0, -1, field);
                case MOVEMENT.LEFT:
                    return checkMovement(-1, 0, field);
                case MOVEMENT.RIGHT:
                    return checkMovement(1, 0, field);
                default:
                    return false;
            }
        }
        private bool checkMovement(int x, int y, int[,] field)
        {
            if (this.iCell - y >= 0 && this.iCell - y <= 33 && this.jCell + x >= 0 && this.jCell + x <= 33)
                return field[this.iCell - y, this.jCell + x] == (int)OBJECT_LIST.BLANK ||
                    field[this.iCell - y, this.jCell + x] == (int)OBJECT_LIST.DOT;
            else
            {
               Teleport?.Invoke(this, EventArgs.Empty);
               return true;
            }

        }

        public void updatePacmanCell(int[,] field)
        {
            switch (moveDirection)
            {
                case MOVEMENT.UP:
                    changePacmanCell(0, 1, field);
                    break;
                case MOVEMENT.DOWN:
                    changePacmanCell(0, -1, field);
                    break;
                case MOVEMENT.LEFT:
                    changePacmanCell(-1, 0, field);
                    break;
                case MOVEMENT.RIGHT:
                    changePacmanCell(1, 0, field);
                    break;
            }
        }

        public void teleportationSetup(int[,] field)
        {
            switch (moveDirection)
            {
                case MOVEMENT.UP:
                    teleportation(0, 1, field);
                    break;
                case MOVEMENT.DOWN:
                    teleportation(0, -1, field);
                    break;
                case MOVEMENT.LEFT:
                    teleportation(-1, 0, field);
                    break;
                case MOVEMENT.RIGHT:
                    teleportation(1, 0, field);
                    break;
            }
        }

        public void teleportation(int x, int y, int[,] field)
        {
            {
                if (this.jCell == 0)
                {
                    field[this.iCell, this.jCell] = 0;
                    field[this.iCell, 32] = 5;
                    this.iCell -= y;
                    this.jCell = 32;
                }
                else if (this.jCell == 33)
                {
                    field[this.iCell, this.jCell] = 0;
                    field[this.iCell, 1] = 5;
                    this.iCell -= y;
                    this.jCell = 1;
                }
                else if (this.iCell == 0)
                {
                    field[this.iCell, this.jCell] = 0;
                    field[32, this.jCell] = 5;
                    this.iCell = 32;
                    this.jCell += x;
                }
                else if (this.iCell == 33)
                {
                    field[this.iCell, this.jCell] = 0;
                    field[1, this.jCell] = 5;
                    this.iCell = 1;
                    this.jCell += x;
                }

            }
        }

        private void changePacmanCell(int x, int y, int[,] field)
        {
            field[this.iCell, this.jCell] = 0;
            field[this.iCell - y, this.jCell + x] = 5;
            this.iCell -= y;
            this.jCell += x;

        }
    }


    public enum MOVEMENT { UP, DOWN, LEFT, RIGHT, NONE};
}
