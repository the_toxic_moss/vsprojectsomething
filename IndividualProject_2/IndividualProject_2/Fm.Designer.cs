﻿
namespace IndividualProject_2
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btn_start = new System.Windows.Forms.Button();
            this.btn_plus = new System.Windows.Forms.Button();
            this.btn_minus = new System.Windows.Forms.Button();
            this.la_size = new System.Windows.Forms.Label();
            this.la_pr = new System.Windows.Forms.Label();
            this.btn_min_pr = new System.Windows.Forms.Button();
            this.btn_plus_pr = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.la_win_or_lose = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(607, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Изменить размерность";
            // 
            // btn_start
            // 
            this.btn_start.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_start.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_start.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_start.Location = new System.Drawing.Point(619, 26);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(223, 46);
            this.btn_start.TabIndex = 1;
            this.btn_start.Text = "Начать игру";
            this.btn_start.UseVisualStyleBackColor = false;
            // 
            // btn_plus
            // 
            this.btn_plus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_plus.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_plus.Font = new System.Drawing.Font("Microsoft YaHei", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_plus.Location = new System.Drawing.Point(619, 164);
            this.btn_plus.Name = "btn_plus";
            this.btn_plus.Size = new System.Drawing.Size(60, 60);
            this.btn_plus.TabIndex = 2;
            this.btn_plus.Text = "+";
            this.btn_plus.UseVisualStyleBackColor = false;
            // 
            // btn_minus
            // 
            this.btn_minus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_minus.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_minus.Font = new System.Drawing.Font("Microsoft YaHei", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_minus.Location = new System.Drawing.Point(782, 164);
            this.btn_minus.Name = "btn_minus";
            this.btn_minus.Size = new System.Drawing.Size(60, 60);
            this.btn_minus.TabIndex = 3;
            this.btn_minus.Text = "–";
            this.btn_minus.UseVisualStyleBackColor = false;
            // 
            // la_size
            // 
            this.la_size.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.la_size.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.la_size.Location = new System.Drawing.Point(706, 164);
            this.la_size.Name = "la_size";
            this.la_size.Size = new System.Drawing.Size(48, 60);
            this.la_size.TabIndex = 4;
            this.la_size.Text = "label2";
            this.la_size.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // la_pr
            // 
            this.la_pr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.la_pr.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.la_pr.Location = new System.Drawing.Point(706, 313);
            this.la_pr.Name = "la_pr";
            this.la_pr.Size = new System.Drawing.Size(48, 60);
            this.la_pr.TabIndex = 8;
            this.la_pr.Text = "label2";
            this.la_pr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_min_pr
            // 
            this.btn_min_pr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_min_pr.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_min_pr.Font = new System.Drawing.Font("Microsoft YaHei", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_min_pr.Location = new System.Drawing.Point(782, 313);
            this.btn_min_pr.Name = "btn_min_pr";
            this.btn_min_pr.Size = new System.Drawing.Size(60, 60);
            this.btn_min_pr.TabIndex = 7;
            this.btn_min_pr.Text = "–";
            this.btn_min_pr.UseVisualStyleBackColor = false;
            // 
            // btn_plus_pr
            // 
            this.btn_plus_pr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_plus_pr.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_plus_pr.Font = new System.Drawing.Font("Microsoft YaHei", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_plus_pr.Location = new System.Drawing.Point(619, 313);
            this.btn_plus_pr.Name = "btn_plus_pr";
            this.btn_plus_pr.Size = new System.Drawing.Size(60, 60);
            this.btn_plus_pr.TabIndex = 6;
            this.btn_plus_pr.Text = "+";
            this.btn_plus_pr.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(616, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(228, 26);
            this.label3.TabIndex = 5;
            this.label3.Text = "Шанс найти бомбу (%)";
            // 
            // la_win_or_lose
            // 
            this.la_win_or_lose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.la_win_or_lose.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.la_win_or_lose.Location = new System.Drawing.Point(619, 468);
            this.la_win_or_lose.Name = "la_win_or_lose";
            this.la_win_or_lose.Size = new System.Drawing.Size(223, 23);
            this.la_win_or_lose.TabIndex = 9;
            this.la_win_or_lose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.la_win_or_lose);
            this.Controls.Add(this.la_pr);
            this.Controls.Add(this.btn_min_pr);
            this.Controls.Add(this.btn_plus_pr);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.la_size);
            this.Controls.Add(this.btn_minus);
            this.Controls.Add(this.btn_plus);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.label1);
            this.Name = "Fm";
            this.Text = "Сапер 2000";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Button btn_plus;
        private System.Windows.Forms.Button btn_minus;
        private System.Windows.Forms.Label la_size;
        private System.Windows.Forms.Label la_pr;
        private System.Windows.Forms.Button btn_min_pr;
        private System.Windows.Forms.Button btn_plus_pr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label la_win_or_lose;
    }
}

