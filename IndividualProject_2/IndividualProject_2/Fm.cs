﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IndividualProject_2
{
    public partial class Fm : Form
    {
        Game game;

        public Fm()
        {
            InitializeComponent();
            game = new Game(this.Height);
            game.GenerateField(Controls);
            this.Resize += (s, e) => game.changeSize(Controls, this.Height);
            game.newSize = game.size;
            la_size.Text = $"{game.newSize}";
            la_pr.Text = $"{game.bombPercent}";
            btn_plus.Click += Btn_plus_Click;
            btn_minus.Click += Btn_minus_Click;
            btn_start.Click += Btn_start_Click;
            btn_min_pr.Click += Btn_min_pr_Click;
            btn_plus_pr.Click += Btn_plus_pr_Click;
            game.Lose += Game_Lose;
            game.Win += Game_Win;

        }

        private void Game_Win(object sender, EventArgs e)
        {
            la_win_or_lose.Text = "Вы победили!";
            la_win_or_lose.Font = new Font("Times New Roman", 16, FontStyle.Bold);
            la_win_or_lose.ForeColor = Color.Green;
        }

        private void Btn_plus_pr_Click(object sender, EventArgs e)
        {
            if (game.bombPercent < 100)
            {
                game.bombPercent += 5;
                la_pr.Text = $"{game.bombPercent}";
            }
        }

        private void Btn_min_pr_Click(object sender, EventArgs e)
        {
            if (game.bombPercent > 5)
            {
                game.bombPercent -= 5;
                la_pr.Text = $"{game.bombPercent}";
            }
        }

        private void Btn_start_Click(object sender, EventArgs e)
        {
            game.GenerateField(Controls);
            game.changeSize(Controls, this.Height);
        }

        private void Game_Lose(object sender, EventArgs e)
        {
            game.lockCells(Controls);
            la_win_or_lose.Text = "Вы проиграли!";
            la_win_or_lose.Font = new Font("Times New Roman", 16, FontStyle.Bold);
            la_win_or_lose.ForeColor = Color.Red;
        }

        private void Btn_minus_Click(object sender, EventArgs e)
        {
            if (game.newSize != 2)
            {
                game.newSize -= 2;
                la_size.Text = $"{game.newSize}";
            }
        }

        private void Btn_plus_Click(object sender, EventArgs e)
        {
            game.newSize += 2;
            la_size.Text = $"{game.newSize}";
        }
    }
}

