﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IndividualProject_2
{
    public class Field : Button
    {
        public bool isBomb;
    }
    class Game
    {
        public int size = 10;
        public int newSize = 10;
        public int oldSize = 0;
        private int formHeight;
        public int offset = 50;
        public int bombPercent = 30;
        Field[,] field;
        public event EventHandler Lose;
        public event EventHandler Win;

        public Game( int h)
        {
            field = new Field[size, size];
            formHeight = h;
        }


        public void regenerateField()
        {
            field = new Field[size, size];
        }
        public void GenerateField( Control.ControlCollection c)
        {
            if (oldSize != 0)
            {
            for (int y = 0; y < oldSize; y++)
                {
                    for (int x = 0; x < oldSize; x++)
                    {
                        c.Remove(field[x, y]);
                    }
                }
            }

            size = newSize;

            regenerateField();
            Random random = new Random();
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    Field newButton = new Field();
                    newButton.Location = new Point(x * (formHeight - offset) / size, y * (formHeight - offset)/ size);
                    newButton.Size = new Size((formHeight - offset)/ size, (formHeight - offset) / size);
                    newButton.Font = new Font("Times New Roman", 16);

                    if (random.Next(0, 100) <= bombPercent)
                    {
                        newButton.isBomb = true;
                    }

                    c.Add(newButton);
                    newButton.MouseUp += new MouseEventHandler(FieldClick); 
                    field[x, y] = newButton;
                }
            }

            oldSize = size;
        }

        public void changeSize(Control.ControlCollection c, int h)
        {
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    field[x, y].Location = new Point(x * (h - offset) / size, y * (h - offset) / size);
                    field[x, y].Size = new Size((h - offset) / size, (h - offset) / size);
                    field[x, y].Font = new Font("Times New Roman", 160 / size);
                }
            }
        }

        public void lockCells(Control.ControlCollection c)
        {
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    field[x, y].Enabled = false;
                }
            }
        }

        public void unlockCells(Control.ControlCollection c)
        {
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    field[x, y].Enabled = true;
                }
            }
        }

        private void FieldClick(object sender, MouseEventArgs e)
        {
            Field clickedButton = (Field)sender;
            if (e.Button == MouseButtons.Left)
            {
                if (clickedButton.isBomb)
                {
                    explode();
                }
                else
                {
                    EmptyField(clickedButton);
                }
            }
        }

        private void EmptyField(Field clickedButton)
        {
            int bombsAround = 0;
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    if (field[x, y] == clickedButton)
                    {
                        bombsAround = countBombs(x, y);
                    }
                }
            }
            if (bombsAround != 0)
            {
                clickedButton.Text = "" + bombsAround;
            }
            clickedButton.Enabled = false;
            checkWin();
        }


        private void checkWin()
        {
            int counter = 0;
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    if (field[x, y].Enabled == true)
                        counter++;
                }
            }

            if (counter == 0)
                Win?.Invoke(this, EventArgs.Empty);

        }

        private void explode()
        {
            foreach (Field button in field)
            {
                if (button.isBomb)
                {
                    button.Text = "X";
                    button.Enabled = false;
                }
            }
            Lose?.Invoke(this, EventArgs.Empty);
            
        }

        private int countBombs(int i, int j)
        {
            int bombs = 0;
            for (int x = i - 1; x <= i + 1; x++)
            {
                for (int y = j - 1; y <= j + 1; y++)
                {
                    if (x >= 0 && x < size && y >= 0 && y < size)
                    {
                        if (field[x, y].isBomb == true)
                        {
                            bombs++;
                        }
                    }
                }
            }
            return bombs; ;
        }
    }

}
