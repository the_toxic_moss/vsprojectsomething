﻿
namespace labAsyncAwaiting
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bu1 = new System.Windows.Forms.Button();
            this.bu2 = new System.Windows.Forms.Button();
            this.bu3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bu1
            // 
            this.bu1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.bu1.Location = new System.Drawing.Point(51, 45);
            this.bu1.Name = "bu1";
            this.bu1.Size = new System.Drawing.Size(266, 86);
            this.bu1.TabIndex = 0;
            this.bu1.Text = "Кнопка раз";
            this.bu1.UseVisualStyleBackColor = true;
            // 
            // bu2
            // 
            this.bu2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.bu2.Location = new System.Drawing.Point(51, 154);
            this.bu2.Name = "bu2";
            this.bu2.Size = new System.Drawing.Size(266, 86);
            this.bu2.TabIndex = 1;
            this.bu2.Text = "Кнопка два";
            this.bu2.UseVisualStyleBackColor = true;
            // 
            // bu3
            // 
            this.bu3.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.bu3.Location = new System.Drawing.Point(373, 45);
            this.bu3.Name = "bu3";
            this.bu3.Size = new System.Drawing.Size(266, 195);
            this.bu3.TabIndex = 2;
            this.bu3.Text = "Кнопка три";
            this.bu3.UseVisualStyleBackColor = true;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 278);
            this.Controls.Add(this.bu3);
            this.Controls.Add(this.bu2);
            this.Controls.Add(this.bu1);
            this.Name = "Fm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bu1;
        private System.Windows.Forms.Button bu2;
        private System.Windows.Forms.Button bu3;
    }
}

