﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labAsyncAwaiting
{
    public partial class Fm : Form
    {
        public Fm()
        {
            InitializeComponent();

            bu1.Click += Bu1_Click;
            bu2.Click += Bu2_Click;
            bu3.Click += Bu3_Click;
        }

        async private void Bu3_Click(object sender, EventArgs e)
        {
            bu3.Enabled = false;
            for (int i = 0; i < 100; i++)
            {
                await Task.Delay(100);
                bu3.Text = i.ToString();
            }
            bu3.Enabled = true;
            bu3.Text = "Завершено";
        }

        async private void Bu2_Click(object sender, EventArgs e)
        {
            bu2.Text = "Wait";
            await Task.Delay(3500);
            bu2.Text = DateTime.Now.ToString();
        }

        async private void Bu1_Click(object sender, EventArgs e)
        {
            bu1.Text = "Жди...";
            Thread.Sleep(2000);
            bu1.Text = DateTime.Now.ToString();
        }
    }
}
