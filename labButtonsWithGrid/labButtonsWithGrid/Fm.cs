﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labButtonsWithGrid
{
    public partial class Fm : Form
    {
        private Button[,] btt;
        private Label[] laa;
        public int Rows { get; private set; } = 5;
        public int Cols { get; private set; } = 5;
        public Fm()
        {
            KeyPreview = true;
            InitializeComponent();
            this.KeyDown += Fm_KeyDown;
            this.Resize += (s, e) => GiveSize();
            CreateButtons();
            GiveSize();

        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W:
                    DeleteButtons();
                    ++Rows;
                    CreateButtons();
                    GiveSize();
                    break;
                case Keys.S:
                    if (Rows != 1)
                    {
                        DeleteButtons();
                        --Rows;
                        CreateButtons();
                        GiveSize();
                    }
                    break;
                case Keys.A:
                    if (Cols != 1)
                    {
                        DeleteButtons();
                        --Cols;
                        CreateButtons();
                        GiveSize();
                    }
                    break;
                case Keys.D:
                    DeleteButtons();
                    ++Cols;
                    CreateButtons();
                    GiveSize();
                    break;
            }
        }

        private void CreateButtons()
        {
            btt = new Button[Rows, Cols];
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    btt[i, j] = new Button();
                    btt[i, j].Text = $"{i * Cols + j + 1}";
                    btt[i, j].Font = new Font("Times New Roman", 14);
                    btt[i, j].MouseDown += onMouseDown;
                    this.Controls.Add(btt[i, j]);
                }
        }

        private void GiveSize()
        {
            int width = this.ClientSize.Width / Cols;
            int height = (this.ClientSize.Height - 100) / Rows;
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    btt[i, j].Width = width;
                    btt[i, j].Height = height;
                    btt[i, j].Location = new Point(j + (j * width), 100 + i + (i * height));
                    this.Controls.Add(btt[i, j]);
                }
        }

        private void DeleteButtons()
        {
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    this.Controls.Remove(btt[i, j]);
                }
        }

        private void onMouseDown(object sender, MouseEventArgs e)
        {
            var sendButton = (Button)sender;
            switch (e.Button)
            {
                case MouseButtons.Left:
                    Random rnd = new Random();
                    sendButton.BackColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                    sendButton.ForeColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                    break;
                case MouseButtons.Right:
                    sendButton.BackColor = Color.FromArgb(1, 1, 1);
                    sendButton.ForeColor = Color.FromArgb(0, 0, 0);
                    sendButton.UseVisualStyleBackColor = true;
                    break;
            }
        }


    }
}
