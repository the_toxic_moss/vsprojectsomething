﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labCreateControlls
{
    public partial class Fm : Form
    {
        public Fm()
        {
            InitializeComponent();
            this.MouseDown += Fm_MouseDown;
        }

        private void Fm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var label = new Label();
                label.Location = e.Location;
                label.Text = $"Текущая координата: {e.X}, {e.Y}";
                label.BackColor = Color.White;
                label.AutoSize = true;
                this.Controls.Add(label);
            }
            if (e.Button == MouseButtons.Right)
            {
                var rnd = new Random();
                for (int i = 0; i < 10; i++)
                {
                    var label = new Label();
                    label.Location = new Point(rnd.Next(ClientSize.Width), rnd.Next(ClientSize.Height));
                    label.Text = $"Текущая координата: {label.Location.X}, {label.Location.Y}";
                    label.BackColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                    label.AutoSize = true;
                    this.Controls.Add(label);
                }
            }
        }
    }
}
