﻿using System;
using System.Collections.Generic;

namespace labDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> x = new Dictionary<int, string>();
            x.Add(5, "Сочи");
            x.Add(10, "Владикавказ");
            x.Add(15, "Южно Сахалинск");
            x.Add(20, "Лондон");
            x.Add(30, "Киров");


            x[10] = "Курск";
            x.Remove(20);

            foreach (KeyValuePair<int, string> v in x)
            {
                Console.WriteLine($"Key = {v.Key}, Value = {v.Value}");
            }
            Console.WriteLine("* * *");
            Console.WriteLine(x[5]);
        }
    }
}
