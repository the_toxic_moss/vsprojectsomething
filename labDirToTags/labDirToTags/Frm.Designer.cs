﻿
namespace labDirToTags
{
    partial class Frm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.forDirs = new System.Windows.Forms.TextBox();
            this.forTags = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // forDirs
            // 
            this.forDirs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.forDirs.Location = new System.Drawing.Point(12, 12);
            this.forDirs.Name = "forDirs";
            this.forDirs.Size = new System.Drawing.Size(776, 23);
            this.forDirs.TabIndex = 0;
            // 
            // forTags
            // 
            this.forTags.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.forTags.Location = new System.Drawing.Point(12, 52);
            this.forTags.Multiline = true;
            this.forTags.Name = "forTags";
            this.forTags.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.forTags.Size = new System.Drawing.Size(776, 364);
            this.forTags.TabIndex = 2;
            // 
            // Frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.forTags);
            this.Controls.Add(this.forDirs);
            this.Name = "Frm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox forDirs;
        private System.Windows.Forms.TextBox forTags;
    }
}

