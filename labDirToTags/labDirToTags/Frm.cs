﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labDirToTags
{
    public partial class Frm : Form
    {
        private List<string> tags = new List<string>();

        public Frm()
        {
            InitializeComponent();

            var s = getTags(Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).FullName).FullName);
            s = s.Select(v => v.TrimStart(new char[] { '(', '"' })
                .TrimEnd(new char[] { ')', '"', ',' })).ToArray();
            forTags.Text = string.Join(Environment.NewLine, s);
        }
        private string[] getTags(string path)
        {
            string[] arr = path.Split(@"\");
            foreach (var item in arr)
            {
                if (!tags.Contains(item))
                    tags.Add(item);
            }
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            DirectoryInfo[] dirs = dirInfo.GetDirectories();
            foreach (DirectoryInfo dir in dirs)
            {
                if (!tags.Contains(dir.Name))
                    tags.Add(dir.Name);
                getTags(dir.FullName);
            }
            return tags.ToArray();
        }
    }
}
