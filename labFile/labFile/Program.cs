﻿using System;
using System.Collections.Generic;
using System.IO;

namespace labFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "test.txt");

            var list = new List<string> { "Red", "White", "Black", "Blue" };

            File.WriteAllText(path, string.Join(Environment.NewLine, list));

            var xReadText = File.ReadAllText(path);
            Console.WriteLine(xReadText);

            Console.WriteLine(File.Exists(path));

            File.Delete(path);

            FileInfo fileInfo = new FileInfo(path);
            Console.WriteLine(fileInfo.FullName);
        }
    }
}
