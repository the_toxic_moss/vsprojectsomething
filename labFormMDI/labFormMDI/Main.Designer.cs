﻿
namespace labFormMDI
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.miCreateNewForm = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindows = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsCascase = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsTileHorizontal = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsTileVertical = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsArrangeIcons = new System.Windows.Forms.ToolStripMenuItem();
            this.miCloseActiveForm = new System.Windows.Forms.ToolStripMenuItem();
            this.miCloseAllForms = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCreateNewForm,
            this.miWindows,
            this.miCloseActiveForm,
            this.miCloseAllForms});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(800, 24);
            this.menuStrip.TabIndex = 2;
            this.menuStrip.Text = "menuStrip1";
            // 
            // miCreateNewForm
            // 
            this.miCreateNewForm.Name = "miCreateNewForm";
            this.miCreateNewForm.Size = new System.Drawing.Size(109, 20);
            this.miCreateNewForm.Text = "Create new Form";
            // 
            // miWindows
            // 
            this.miWindows.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miWindowsCascase,
            this.miWindowsTileHorizontal,
            this.miWindowsTileVertical,
            this.miWindowsArrangeIcons});
            this.miWindows.Name = "miWindows";
            this.miWindows.Size = new System.Drawing.Size(68, 20);
            this.miWindows.Text = "Windows";
            // 
            // miWindowsCascase
            // 
            this.miWindowsCascase.Name = "miWindowsCascase";
            this.miWindowsCascase.Size = new System.Drawing.Size(147, 22);
            this.miWindowsCascase.Text = "Cascade";
            // 
            // miWindowsTileHorizontal
            // 
            this.miWindowsTileHorizontal.Name = "miWindowsTileHorizontal";
            this.miWindowsTileHorizontal.Size = new System.Drawing.Size(147, 22);
            this.miWindowsTileHorizontal.Text = "TileHorizontal";
            // 
            // miWindowsTileVertical
            // 
            this.miWindowsTileVertical.Name = "miWindowsTileVertical";
            this.miWindowsTileVertical.Size = new System.Drawing.Size(147, 22);
            this.miWindowsTileVertical.Text = "TileVertical";
            // 
            // miWindowsArrangeIcons
            // 
            this.miWindowsArrangeIcons.Name = "miWindowsArrangeIcons";
            this.miWindowsArrangeIcons.Size = new System.Drawing.Size(147, 22);
            this.miWindowsArrangeIcons.Text = "ArrangeIcons";
            // 
            // miCloseActiveForm
            // 
            this.miCloseActiveForm.Name = "miCloseActiveForm";
            this.miCloseActiveForm.Size = new System.Drawing.Size(115, 20);
            this.miCloseActiveForm.Text = "Close Active Form";
            // 
            // miCloseAllForms
            // 
            this.miCloseAllForms.Name = "miCloseAllForms";
            this.miCloseAllForms.Size = new System.Drawing.Size(99, 20);
            this.miCloseAllForms.Text = "Close all Forms";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip);
            this.Name = "Main";
            this.Text = "Form1";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem miCreateNewForm;
        private System.Windows.Forms.ToolStripMenuItem miWindows;
        private System.Windows.Forms.ToolStripMenuItem miWindowsCascase;
        private System.Windows.Forms.ToolStripMenuItem miWindowsTileHorizontal;
        private System.Windows.Forms.ToolStripMenuItem miWindowsTileVertical;
        private System.Windows.Forms.ToolStripMenuItem miWindowsArrangeIcons;
        private System.Windows.Forms.ToolStripMenuItem miCloseActiveForm;
        private System.Windows.Forms.ToolStripMenuItem miCloseAllForms;
    }
}

