﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFormMDI
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();

            this.IsMdiContainer = true;

            miCreateNewForm.Click += MiCreateNewForm_Click;

            miWindowsCascase.Click += (s, e) => this.LayoutMdi(MdiLayout.Cascade);
            miWindowsTileHorizontal.Click += (s, e) => this.LayoutMdi(MdiLayout.TileHorizontal);
            miWindowsTileVertical.Click += (s, e) => this.LayoutMdi(MdiLayout.TileVertical);
            miWindowsArrangeIcons.Click += (s, e) => this.LayoutMdi(MdiLayout.ArrangeIcons);

            miCloseActiveForm.Click += (s, e) => this.ActiveMdiChild?.Close();
            miCloseAllForms.Click += delegate
            {
                while (this.MdiChildren.Count() > 0)
                {
                    this.MdiChildren[0].Close();
                }
            };
        }
        private void MiCreateNewForm_Click(object sender, EventArgs e)
        {
            var x = new Notes();
            x.MdiParent = this;
            x.Show();
        }

    }
}
