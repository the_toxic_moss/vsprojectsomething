﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFormSDI
{
    public partial class Main : Form
    {
        private Optims obrazec = new Optims();
        public Main()
        {
            InitializeComponent();

            button1.Click += (s, e) => new Optims().Show();

            button2.Click += (s, e) => new Optims().ShowDialog();

            button3.Click += (s, e) => obrazec.Show();
        }

    }
}
