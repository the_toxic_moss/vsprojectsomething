﻿
namespace labGenPassword
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ckTemplate = new System.Windows.Forms.CheckBox();
            this.edLength = new System.Windows.Forms.NumericUpDown();
            this.ckSpecial = new System.Windows.Forms.CheckBox();
            this.ckNumber = new System.Windows.Forms.CheckBox();
            this.ckUpper = new System.Windows.Forms.CheckBox();
            this.ckLower = new System.Windows.Forms.CheckBox();
            this.buGenerate = new System.Windows.Forms.Button();
            this.edPassword = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.edLength)).BeginInit();
            this.SuspendLayout();
            // 
            // ckTemplate
            // 
            this.ckTemplate.AutoSize = true;
            this.ckTemplate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ckTemplate.Location = new System.Drawing.Point(37, 360);
            this.ckTemplate.Name = "ckTemplate";
            this.ckTemplate.Size = new System.Drawing.Size(117, 25);
            this.ckTemplate.TabIndex = 12;
            this.ckTemplate.Text = "По шаблону";
            this.ckTemplate.UseVisualStyleBackColor = true;
            // 
            // edLength
            // 
            this.edLength.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.edLength.Location = new System.Drawing.Point(37, 405);
            this.edLength.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.edLength.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.edLength.Name = "edLength";
            this.edLength.Size = new System.Drawing.Size(120, 29);
            this.edLength.TabIndex = 14;
            this.edLength.Value = new decimal(new int[] {
            14,
            0,
            0,
            0});
            // 
            // ckSpecial
            // 
            this.ckSpecial.AutoSize = true;
            this.ckSpecial.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ckSpecial.Location = new System.Drawing.Point(37, 315);
            this.ckSpecial.Name = "ckSpecial";
            this.ckSpecial.Size = new System.Drawing.Size(194, 25);
            this.ckSpecial.TabIndex = 13;
            this.ckSpecial.Text = "Специальные символы";
            this.ckSpecial.UseVisualStyleBackColor = true;
            // 
            // ckNumber
            // 
            this.ckNumber.AutoSize = true;
            this.ckNumber.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ckNumber.Location = new System.Drawing.Point(37, 270);
            this.ckNumber.Name = "ckNumber";
            this.ckNumber.Size = new System.Drawing.Size(81, 25);
            this.ckNumber.TabIndex = 11;
            this.ckNumber.Text = "Цифры";
            this.ckNumber.UseVisualStyleBackColor = true;
            // 
            // ckUpper
            // 
            this.ckUpper.AutoSize = true;
            this.ckUpper.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ckUpper.Location = new System.Drawing.Point(37, 225);
            this.ckUpper.Name = "ckUpper";
            this.ckUpper.Size = new System.Drawing.Size(238, 25);
            this.ckUpper.TabIndex = 10;
            this.ckUpper.Text = "Символы в верхнем регистре";
            this.ckUpper.UseVisualStyleBackColor = true;
            // 
            // ckLower
            // 
            this.ckLower.AutoSize = true;
            this.ckLower.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ckLower.Location = new System.Drawing.Point(37, 180);
            this.ckLower.Name = "ckLower";
            this.ckLower.Size = new System.Drawing.Size(236, 25);
            this.ckLower.TabIndex = 9;
            this.ckLower.Text = "Символы в нижнем регистре";
            this.ckLower.UseVisualStyleBackColor = true;
            // 
            // buGenerate
            // 
            this.buGenerate.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buGenerate.Location = new System.Drawing.Point(111, 83);
            this.buGenerate.Name = "buGenerate";
            this.buGenerate.Size = new System.Drawing.Size(213, 66);
            this.buGenerate.TabIndex = 8;
            this.buGenerate.Text = "Сгенерировать";
            this.buGenerate.UseVisualStyleBackColor = true;
            // 
            // edPassword
            // 
            this.edPassword.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.edPassword.Location = new System.Drawing.Point(37, 31);
            this.edPassword.Name = "edPassword";
            this.edPassword.Size = new System.Drawing.Size(360, 29);
            this.edPassword.TabIndex = 7;
            this.edPassword.Text = "<Password>";
            this.edPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 526);
            this.Controls.Add(this.ckTemplate);
            this.Controls.Add(this.edLength);
            this.Controls.Add(this.ckSpecial);
            this.Controls.Add(this.ckNumber);
            this.Controls.Add(this.ckUpper);
            this.Controls.Add(this.ckLower);
            this.Controls.Add(this.buGenerate);
            this.Controls.Add(this.edPassword);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.edLength)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox ckTemplate;
        private System.Windows.Forms.NumericUpDown edLength;
        private System.Windows.Forms.CheckBox ckSpecial;
        private System.Windows.Forms.CheckBox ckNumber;
        private System.Windows.Forms.CheckBox ckUpper;
        private System.Windows.Forms.CheckBox ckLower;
        private System.Windows.Forms.Button buGenerate;
        private System.Windows.Forms.TextBox edPassword;
    }
}

