﻿using labGenPassword_Core;
using System;

namespace labGenPasswordCNS
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.WriteLine("Введите количество паролей");
                int count = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Создать пароли согласно шаблону? (пример шаблона: 12345?!trghQFHS)");
                string template = Console.ReadLine();
                int length = 1;
                string lower = "", upper = "", number = "", special = "";
                if (template == "нет")
                {
                    Console.WriteLine("Введите длину паролей");
                    length = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Включить символы в нижнем регистре? (да/нет)");
                    lower = Console.ReadLine();
                    Console.WriteLine("Включить символы в верхнем регистре? (да/нет)");
                    upper = Console.ReadLine();
                    Console.WriteLine("Включить цифры? (да/нет)");
                    number = Console.ReadLine();
                    Console.WriteLine("Включить специальные символы? (да/нет)");
                    special = Console.ReadLine();
                }
                for (int i = 1; i <= count; i++)
                {
                    Console.WriteLine(Utils.RandomStr(length, lower == "да" ? true : false,
                    upper == "да" ? true : false, number == "да" ? true : false, special == "да" ? true : false, template == "да" ? true : false));
                }
                Console.WriteLine("Продолжить? (да/нет)");
                string regenerate = Console.ReadLine();
                if (regenerate == "нет")
                    break;
            }
        }
    }
}
