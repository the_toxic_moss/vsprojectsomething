﻿using System;
using System.Text;

namespace labGenPassword_Core
{
    public class Utils
    {
        public static string RandomStr(int length, bool lower, bool upper, bool number, bool special, bool template)
        {
            string c1 = "abcdefghijklmnopqrstuvwxyz";
            string c2 = "0123456789";
            string c3 = "~^<>-*$#!?/;";

            var symList = new StringBuilder();
            var symListResult = new StringBuilder();
            var rnd = new Random();

            if (lower) symList.Append(c1);
            if (upper) symList.Append(c1.ToUpper());
            if (number) symList.Append(c2);
            if (special) symList.Append(c3);

            if (symList.ToString() == "") symList.Append(c1);

            //пример шаблона: 12345?!trghQFHS
            if (template)
            {
                //первые пять цифр
                for (int i = 1; i <= 5; i++)
                {
                    symListResult.Append(c2[rnd.Next(c2.Length)]);
                }
                //два спец символа
                for (int i = 1; i <= 2; i++)
                {
                    symListResult.Append(c3[rnd.Next(c3.Length)]);
                }
                //четыре буквы (lower)
                for (int i = 1; i <= 4; i++)
                {
                    symListResult.Append(c1[rnd.Next(c1.Length)]);
                }
                //четыре буквы (upper)
                for (int i = 1; i <= 4; i++)
                {
                    symListResult.Append(c1.ToUpper()[rnd.Next(c1.Length)]);
                }
            }
            else
                while (symListResult.Length < length)
                    symListResult.Append(symList[rnd.Next(symList.Length)]);

            return symListResult.ToString();
        }
    }
}
