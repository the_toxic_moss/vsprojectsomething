﻿
namespace labGraphicsOfFunction
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.trackSpecial = new System.Windows.Forms.TrackBar();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.funcList = new System.Windows.Forms.ComboBox();
            this.trackB = new System.Windows.Forms.TrackBar();
            this.trackA = new System.Windows.Forms.TrackBar();
            this.holst = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackSpecial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.holst)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.trackSpecial);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.funcList);
            this.panel1.Controls.Add(this.trackB);
            this.panel1.Controls.Add(this.trackA);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(429, 575);
            this.panel1.TabIndex = 1;
            // 
            // trackSpecial
            // 
            this.trackSpecial.Location = new System.Drawing.Point(75, 282);
            this.trackSpecial.Maximum = 30;
            this.trackSpecial.Minimum = -30;
            this.trackSpecial.Name = "trackSpecial";
            this.trackSpecial.Size = new System.Drawing.Size(273, 45);
            this.trackSpecial.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(14, 282);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 24);
            this.label4.TabIndex = 6;
            this.label4.Text = "k";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(14, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "b";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(14, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "a";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(169, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 23);
            this.label1.TabIndex = 3;
            this.label1.Text = "Function";
            // 
            // funcList
            // 
            this.funcList.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.funcList.FormattingEnabled = true;
            this.funcList.Location = new System.Drawing.Point(75, 49);
            this.funcList.Name = "funcList";
            this.funcList.Size = new System.Drawing.Size(273, 27);
            this.funcList.TabIndex = 2;
            // 
            // trackB
            // 
            this.trackB.Location = new System.Drawing.Point(75, 192);
            this.trackB.Maximum = 30;
            this.trackB.Minimum = -30;
            this.trackB.Name = "trackB";
            this.trackB.Size = new System.Drawing.Size(273, 45);
            this.trackB.TabIndex = 1;
            // 
            // trackA
            // 
            this.trackA.Location = new System.Drawing.Point(75, 113);
            this.trackA.Minimum = -10;
            this.trackA.Name = "trackA";
            this.trackA.Size = new System.Drawing.Size(273, 45);
            this.trackA.TabIndex = 0;
            this.trackA.Value = 1;
            // 
            // holst
            // 
            this.holst.BackColor = System.Drawing.SystemColors.Control;
            this.holst.Dock = System.Windows.Forms.DockStyle.Fill;
            this.holst.Location = new System.Drawing.Point(429, 0);
            this.holst.Name = "holst";
            this.holst.Size = new System.Drawing.Size(752, 575);
            this.holst.TabIndex = 2;
            this.holst.TabStop = false;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1181, 575);
            this.Controls.Add(this.holst);
            this.Controls.Add(this.panel1);
            this.Name = "Fm";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackSpecial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.holst)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox funcList;
        private System.Windows.Forms.TrackBar trackB;
        private System.Windows.Forms.TrackBar trackA;
        private System.Windows.Forms.PictureBox holst;
        private System.Windows.Forms.TrackBar trackSpecial;
        private System.Windows.Forms.Label label4;
    }
}

