﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labGraphicsOfFunction
{
    public partial class Fm : Form
    {
        private int a, b, k;
        private List<int> xList = new List<int>();
        private List<int> yList = new List<int>();
        private Bitmap bitmap;
        private Graphics g;

        public Fm()
        {
            InitializeComponent();

            //HW
            //график функции + изменение параметров
            this.DoubleBuffered = true;
            bitmap = new Bitmap(holst.ClientSize.Width, holst.ClientSize.Height);
            g = Graphics.FromImage(bitmap);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            funcList.Items.Add("y = a * x + b * k");
            funcList.Items.Add("y = a * x^2 - b^2 - k");
            funcList.Items.Add("y = a * x^3 + b^2 / k");
            funcList.Items.Add("y = a * x^2 * b * x + k");

            a = trackA.Value;
            b = trackB.Value;
            k = trackSpecial.Value;

            label2.Text = $"a = {a}";
            label3.Text = $"b = {b}";

            trackA.ValueChanged += (s, e) =>
            {
                a = trackA.Value;
                label2.Text = $"{a}";
                changeGraph();
            };
            trackB.ValueChanged += (s, e) =>
            {
                b = trackB.Value;
                label3.Text = $"{b}";
                changeGraph();
            };
            trackSpecial.ValueChanged += (s, e) =>
            {
                k = trackSpecial.Value;
                label4.Text = $"{k}";
                changeGraph();
            };

            funcList.SelectedIndexChanged += (s, e) => changeGraph();
            funcList.SelectedIndex = 0;

            holst.Paint += (s, e) => e.Graphics.DrawImage(bitmap, 0, 0);
            holst.MouseMove += (s, e) => this.Text = $"X: {e.X}, Y: {e.Y}";

            this.Resize += (s, e) =>
            {
                bitmap = new Bitmap(holst.ClientSize.Width, holst.ClientSize.Height);
                g = Graphics.FromImage(bitmap);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                changeGraph();
            };
        }

        private void changeGraph()
        {
            xList.Clear();
            yList.Clear();
            for (int i = -holst.Width / 2; i <= holst.Width / 2; i++)
            {
                xList.Add(i);
                yList.Add(CalcY(i));
            }
            DrawFunction(xList.ToArray(), yList.ToArray());
        }

        private int CalcY(int x)
        {
            int y = 0;
            switch (funcList.SelectedIndex)
            {
                case 0:
                    y = a * x + b * k;
                    break;
                case 1:
                    y = a * (int)Math.Pow(x, 2) - (int)Math.Pow(b, 2) - k;
                    break;
                case 2:
                    y = a * (int)Math.Pow(x, 3) + (int)Math.Pow(b, 2) / k;
                    break;
                case 3:
                    y = a * (int)Math.Pow(x, 3) + b * x + k;
                    break;
            }
            return y;
        }

        private void DrawFunction(int[] xArray, int[] yArray)
        {
            g.Clear(SystemColors.Control);
            g.ResetTransform();
            g.TranslateTransform(holst.Width / 2, holst.Height / 2);

            for (int i = 0; i < xArray.Length; i++)
            {
                if (i != xArray.Length - 1)
                    g.DrawLine(new Pen(Color.Black, 2), new Point(xArray[i], yArray[i]),
                        new Point(xArray[i + 1], yArray[i + 1]));
            }
            
            g.DrawLine(Pens.Gray, new Point(-holst.Width / 2, 0), new Point(holst.Width / 2, 0));
            g.DrawLine(Pens.Gray, new Point(0, -holst.Height / 2), new Point(0, holst.Height / 2));
            holst.Invalidate();
        }

    }
}
