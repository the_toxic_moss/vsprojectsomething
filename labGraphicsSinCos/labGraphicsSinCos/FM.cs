﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace labGraphicsSinCos
{
    
    public partial class FM : Form
    {
        private const int DOT_RADIUS = 2;
        public FM()
        {
            InitializeComponent();

            this.BackgroundImageLayout = ImageLayout.None;
            DrawAll();
            this.Resize += (s, e) => DrawAll();
            this.Text += ": (Sin - красный, Cos - зеленый, Tan - синий)";
        }


        private void DrawAll()
        {
            var b = new Bitmap(this.ClientSize.Width, this.ClientSize.Height);
            var g = Graphics.FromImage(b);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            var grCountWave = 5;
            var grShiftY = b.Height / 2;
            var grHeight = (int) (grShiftY * 0.8);
            var grWidthPI = Math.PI / b.Width;

            g.DrawLine(new Pen(Color.Black), 0, grShiftY, b.Width, grShiftY);
            for (int i = 0; i < b.Width; i++)
            {
                var pointX = b.Width / grCountWave * i;
                g.DrawLine(Pens.Black, pointX, grShiftY - 5, pointX, grShiftY + 5);
                g.DrawString($"{i}π", new Font("Consolas", 10), new SolidBrush(Color.Black), pointX - 20, grShiftY + 5);
            }
            g.DrawLine(new Pen(Color.Black), 0, 0, 0, b.Height);
            g.DrawLine(new Pen(Color.Black), -5, grShiftY - grHeight, 5, grShiftY - grHeight);
            g.DrawLine(new Pen(Color.Black), -5, grShiftY + grHeight, 5, grShiftY + grHeight);

            g.DrawString("+1", new Font("Consolas", 10), new SolidBrush(Color.Black), 10, grShiftY - grHeight - 5);
            g.DrawString("-1", new Font("Consolas", 10), new SolidBrush(Color.Black), 10, grShiftY + grHeight - 5);

            float x;
            float y;

            for (int i = 0; i < b.Width; i++)
            {
                x = i;
                y = (float)(grHeight * Math.Sin(i * grCountWave * grWidthPI) + grShiftY);

                g.FillEllipse(new SolidBrush(Color.Red), PointToRectangle(x, y));

                y = (float)(grHeight * -Math.Cos(i * grCountWave * grWidthPI) + grShiftY);

                g.FillEllipse(new SolidBrush(Color.Green), PointToRectangle(x, y));

                y = (float)(grHeight * -Math.Tan(i * grCountWave * grWidthPI) + grShiftY);
                if (y > 0 && y < b.Height)
                    g.FillEllipse(new SolidBrush(Color.Yellow), PointToRectangle(x, y));

                y = (float)(grHeight * -Math.Sin(i * 5 * grCountWave * grWidthPI) * i / grHeight / 4 + grShiftY);

                g.FillEllipse(new SolidBrush(Color.Orange), PointToRectangle(x, y));
            }

            this.BackgroundImage = (Bitmap)b.Clone();
        }

        private Rectangle PointToRectangle(float x, float y) => PointToRectangle(x, y, DOT_RADIUS);
        private Rectangle PointToRectangle(float x, float y, int r) => new Rectangle((int) x - r, (int) y - r, r * 2, r * 2);
    }
}
