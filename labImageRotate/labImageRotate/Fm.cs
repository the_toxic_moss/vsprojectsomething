﻿using labImageRotate.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageRotate
{
    public partial class Fm : Form
    {
        private Bitmap Imhi;

        public Fm()
        {
            InitializeComponent();

            Imhi = new Bitmap(Resources._1);

            pictureBox1.Paint += PictureBox1_Paint;
            trackBar1.ValueChanged += (s, e) => pictureBox1.Invalidate();

            button1.Click += (s, e) =>
            {
                Imhi.RotateFlip(RotateFlipType.RotateNoneFlipX);
                pictureBox1.Invalidate();
            };

            button2.Click += (s, e) =>
            {
                Imhi.RotateFlip(RotateFlipType.RotateNoneFlipY);
                pictureBox1.Invalidate();
            };

            //помним, что есть еще  Imhi.RotateFlip, pictureBox1.Image.RotateFlip
            

        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.TranslateTransform(Imhi.Width/2, Imhi.Height/2);
            e.Graphics.RotateTransform(trackBar1.Value);
            e.Graphics.DrawImage(Imhi, -Imhi.Width/2, -Imhi.Height/2);

        }
    }
}
