﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollHorz
{
    public partial class Fm : Form
    {
        private Bitmap imgBg;
        private readonly Bitmap b;
        private readonly Graphics g;
        private int dX = 0;
        private Point startPoint;
        private double inc = 1;

        public Fm()
        {
            InitializeComponent();

            imgBg = Properties.Resources.bg;
            this.Height = imgBg.Height;
            this.Width = imgBg.Width;

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => { UpdateBg(); e.Graphics.DrawImage(b, 0, 0); };
            this.MouseDown += (s, e) => startPoint = e.Location;
            this.MouseMove += Fm_MouseMove;
            this.KeyDown += Fm_KeyDown;
            this.KeyUp += Fm_KeyUp;
            this.MouseWheel += Fm_MouseWheel;
            this.MouseUp += Fm_MouseUp;

        }

        private void Fm_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                inc = 1;
        }

        private void Fm_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta > 0)
                UpdateDeltaX(8);
            else
                UpdateDeltaX(-8);
            this.Invalidate();
        }

        private void Fm_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    inc = 1;
                    break;
                case Keys.Right:
                    inc = 1;
                    break;
            }
        }
        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) 
            {
                UpdateDeltaX(e.X - startPoint.X);
                startPoint = e.Location;
                this.Invalidate();
            }
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    UpdateDeltaX(50);
                    break;
                case Keys.Right:
                    UpdateDeltaX(-50);
                    break;
            }
            this.Invalidate();
        }


        private void UpdateDeltaX(int v)
        {
            dX += (int)Math.Floor(v * inc);
            if (inc <= 6)
            inc += 0.1;
            if (dX > 0)
                dX -= imgBg.Width;
            else
                if (dX < -imgBg.Width)
                dX += imgBg.Width;
        }

        private void UpdateBg()
        {
            var imageCount = Math.Floor((double)this.Width / (double)imgBg.Width);
            for (int i = 0; i < imageCount + 2; i++)
                g.DrawImage(imgBg, dX + i * imgBg.Width, 0);
        }
    }
}
