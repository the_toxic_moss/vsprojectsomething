﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollVert
{
    public partial class Fm : Form
    {
        private Bitmap imageBg;
        private Point startPoint;
        private readonly Bitmap b;
        private readonly Graphics g;
        private double inc = 1;
        private int dY = 0;

        public Fm()
        {
            InitializeComponent();

            imageBg = Properties.Resources.bg1;
            this.Width = imageBg.Width;
            this.Height = imageBg.Height;

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => { UpdateBg(); e.Graphics.DrawImage(b, 0, 0); };
            this.MouseDown += (s, e) => startPoint = e.Location;
            this.MouseMove += Fm_MouseMove;
            this.KeyDown += Fm_KeyDown;
            this.KeyUp += Fm_KeyUp;
            this.MouseWheel += Fm_MouseWheel;
            this.MouseUp += Fm_MouseUp;
        }

        private void Fm_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                inc = 1;
        }

        private void Fm_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta > 0)
                UpdateDeltaY(8);
            else
                UpdateDeltaY(-8);
            this.Invalidate();
        }


        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                UpdateDeltaY(e.Y - startPoint.Y);
                startPoint = e.Location;
                this.Invalidate();
            }
        }

        private void Fm_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    inc = 1;
                    break;
                case Keys.Down:
                    inc = 1;
                    break;
            }
        }
        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    UpdateDeltaY(20);
                    break;
                case Keys.Down:
                    UpdateDeltaY(20);
                    break;
            }
            this.Invalidate();
        }

        private void UpdateDeltaY(int v)
        {
            dY += (int)Math.Floor(v * inc);
            if (inc <= 10)
                inc += 0.1;
            if (dY > 0)
                dY -= imageBg.Height;
            else
                if (dY < -imageBg.Height)
                dY += imageBg.Height;
        }

        private void UpdateBg()
        {
            var imageCount = Math.Floor((double)this.Height / (double)imageBg.Height);
            for (int i = 0; i < imageCount + 2; i++)
                g.DrawImage(imageBg, 0, dY + i * imageBg.Height);
        }

    }
}
